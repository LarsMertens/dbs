var App = angular.module('App', ['SQLCtrl', 'SQLService', 'timer'], function($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});