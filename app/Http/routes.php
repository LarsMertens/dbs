<?php

Route::get('/', function () { return view('welcome'); });

// SQL Service Requests
Route::post('/insert/{loop_counter}', 'SQLController@insert');
Route::post('/simpleInsert/{loop_counter}', 'SQLController@simpleInsert');

Route::get('/selectByID/{loop_counter}', 'SQLController@selectByID');
Route::get('/selectByName/{loop_counter}', 'SQLController@selectByName');
Route::get('/selectWherePrice/{loop_counter}', 'SQLController@selectWherePrice');

Route::get('/updateByID/{loop_counter}', 'SQLController@updateByID');
Route::get('/updateByName/{loop_counter}', 'SQLController@updateByName');

Route::get('/joinTables/{loop_counter}', 'SQLController@joinTables');

Route::post('/dropAll', 'SQLController@dropAll');