angular.module('SQLService', [])

    .factory('SQLService', function($http) {

        return {
            insert : function($loop_counter){
                return $http({
                    method: 'POST',
                    url: '/insert/'+$loop_counter
                });
            },
            simpleInsert : function($loop_counter){
                return $http({
                    method: 'POST',
                    url: '/simpleInsert/'+$loop_counter
                });
            },
            selectByID : function($loop_counter){
                return $http({
                    method: 'get',
                    url: '/selectByID/'+$loop_counter
                });
            },
            selectByName : function($loop_counter){
                return $http({
                    method: 'get',
                    url: '/selectByName/'+$loop_counter
                });
            },
            selectWherePrice : function($loop_counter){
                return $http({
                    method: 'get',
                    url: '/selectWherePrice/'+$loop_counter
                });
            },
            updateByID : function($loop_counter){
                return $http({
                    method: 'get',
                    url: '/updateByID/'+$loop_counter
                });
            },
            updateByName : function($loop_counter){
                return $http({
                    method: 'get',
                    url: '/updateByName/'+$loop_counter
                });
            },
            joinTables : function($loop_counter){
                return $http({
                    method: 'get',
                    url: '/joinTables/'+$loop_counter
                });
            },
            dropAll : function(){
                return $http({
                    method: 'POST',
                    url: '/dropAll'
                });
            }
        }

    });