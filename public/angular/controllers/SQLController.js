angular.module('SQLCtrl', [])

.controller('SQLController', function($scope, $http, $timeout, SQLService) {

    $scope.loading = false;

    $scope.insert = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.insert($scope.loop_counter)
            .then(function (success) {
                $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.simpleInsert = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.simpleInsert($scope.loop_counter)
            .then(function (success) {
                $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.selectByID = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.selectByID($scope.loop_counter)
            .then(function (success) {
             $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.selectWherePrice = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.selectWherePrice($scope.loop_counter)
            .then(function (success) {
             $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
    $scope.selectByName = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.selectByName($scope.loop_counter)
            .then(function (success) {
                             $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.updateByID = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.updateByID($scope.loop_counter)
            .then(function (success) {
                             $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.updateByName = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.updateByName($scope.loop_counter)
            .then(function (success) {
 $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.joinTables = function() {
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.joinTables($scope.loop_counter)
            .then(function (success) {
 $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.dropAll = function(){
        $scope.loading = true;
        $scope.$broadcast('timer-clear');
        $scope.$broadcast('timer-start');
        SQLService.dropAll()
            .then(function (success) {
                $scope.$broadcast('timer-stop');
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

});


