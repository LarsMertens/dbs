<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Hash;

class SQLController extends Controller
{

    public function insert($loop_counter){
           for($i = 0; $i < $loop_counter; $i++) {
            $cat_id_1 = DB::table('category')->insertGetId(array(
                'name' => 'My Cat',
                'type' => 'hoofd',
                'main' => 'The Head Cat'
            ));
            $cat_id_2 = DB::table('category')->insertGetId(array(
                'name' => 'Lolz Cat',
                'type' => 'hoofd',
                'main' => 'The Header'
            ));
            $cat_id_3 = DB::table('category')->insertGetId(array(
                'name' => 'Specialities',
                'type' => 'hoofd',
                'main' => 'Very special'
            ));
            $cat_id_4 = DB::table('category')->insertGetId(array(
                'name' => 'Alrighto',
                'type' => 'sub',
                'main' => 'oab'
            ));
            $cat_id_5 = DB::table('category')->insertGetId(array(
                'name' => 'Juistem',
                'type' => 'sub',
                'main' => 'bao'
            ));

            $prod_id_1 = DB::table('product')->insertGetId(array(
                'name' => 'A name',
                'price' => 2.32,
                'btw' => 20
            ));
            $prod_id_2 = DB::table('product')->insertGetId(array(
                'name' => 'To',
                'price' => 1.32,
                'btw' => 6
            ));
            $prod_id_3 = DB::table('product')->insertGetId(array(
                'name' => 'Okidoki',
                'price' => 10.20,
                'btw' => 21
            ));
            $prod_id_4 = DB::table('product')->insertGetId(array(
                'name' => 'HQhewuharojia jejo',
                'price' => 22.00,
                'btw' => 8
            ));
            $prod_id_5 = DB::table('product')->insertGetId(array(
                'name' => 'wjrijatji eji i',
                'price' => 0.32,
                'btw' => 6
            ));

            DB::table('product_category')->insert(array(
                'category_id' => $cat_id_1,
                'product_id' => $prod_id_1
            ));
            DB::table('product_category')->insert(array(
                'category_id' => $cat_id_2,
                'product_id' => $prod_id_2
            ));
            DB::table('product_category')->insert(array(
                'category_id' => $cat_id_3,
                'product_id' => $prod_id_3
            ));
            DB::table('product_category')->insert(array(
                'category_id' => $cat_id_4,
                'product_id' => $prod_id_4
            ));
            DB::table('product_category')->insert(array(
                'category_id' => $cat_id_5,
                'product_id' => $prod_id_5
            ));

            $user_1 = DB::table('user')->insertGetId(array(
                'name' => 'test',
                'email' => 'test@test.nl'.$i,
                'password' => Hash::make('fsfse'),
                'type' => 'user',
            ));
            $user_2 = DB::table('user')->insertGetId(array(
                'name' => 'bc',
                'email' => 'bc@bc.nl'.$i,
                'password' => Hash::make('sdifisfhd'),
                'type' => 'admin',
            ));
            $user_3 = DB::table('user')->insertGetId(array(
                'name' => 'cd',
                'email' => 'cd@cd.nl'.$i,
                'password' => Hash::make('oooooooooooo'),
                'type' => 'user',
            ));
            $user_4 = DB::table('user')->insertGetId(array(
                'name' => 'ioesist df xnjv',
                'email' => 'df@df.com'.$i,
                'password' => Hash::make('jiop88784'),
                'type' => 'user',
            ));
            $user_5 = DB::table('user')->insertGetId(array(
                'name' => 'are',
                'email' => 'oo@bb.nl'.$i,
                'password' => Hash::make('wakh88'),
                'type' => 'admin',
            ));

            DB::table('naw')->insert(array(
                'user_id' => $user_1,
                'firstname' => 'sdfsdf',
                'insertion' => 'aa',
                'lastname' => 'fdsfdsffsd',
                'phonenumber' => '34839438389',
                'address' => 'jjiresi jjires ',
                'housenumber' => '34',
                'town' => 'hisehrhiser h',
                'zipcode' => '3434AB',
                'email' => 'jierih@mekst.nl'
            ));
            DB::table('naw')->insert(array(
                'user_id' => $user_2,
                'firstname' => 'beresb',
                'insertion' => 'b',
                'lastname' => 'oesrooer',
                'phonenumber' => '3334383812129',
                'address' => 'setesjab ',
                'housenumber' => '12',
                'town' => 'hisehrhiseserer h',
                'zipcode' => '234AB',
                'email' => 'oooo@oooo.nl'
            ));
            DB::table('naw')->insert(array(
                'user_id' => $user_3,
                'firstname' => 'serjijres',
                'insertion' => 'bbwar',
                'lastname' => 'bjiaji',
                'phonenumber' => '3434',
                'address' =>  'dfjgjgf',
                'housenumber' => '344',
                'town' => 'hooser h',
                'zipcode' => '3403 OB',
                'email' => 'ppbammo@mekst.nl'
            ));
            DB::table('naw')->insert(array(
                'user_id' => $user_4,
                'firstname' => 'sdfsdf',
                'insertion' => 'ba',
                'lastname' => 'fdfsfdsffsd',
                'phonenumber' => '2323',
                'address' => 'oooi jjires ',
                'housenumber' => '13',
                'town' => 'hiooseer h',
                'zipcode' => '1123BV',
                'email' => 'oaobaj@mekst.nl'
            ));
            DB::table('naw')->insert(array(
                'user_id' => $user_5,
                'firstname' => 'sdfsfggsdf',
                'insertion' => 'aa',
                'lastname' => 'fabfdfsfssffsd',
                'phonenumber' => '454',
                'address' => 'jjsfesi jjires ',
                'housenumber' => '34',
                'town' => 'hiseh23iser h',
                'zipcode' => '34234AB',
                'email' => 'ababa@mekst.nl'
            ));

            $invoice_1 = DB::table('invoice')->insertGetId(array(
                'user_id' => $user_1,
                'price' => 20.50,
                'status' => 'SUCCESS',
                'description' => 'abjbiawj bbaiwr ubahwrhuh awrhhuawr haiwhr',
            ));
            $invoice_2 = DB::table('invoice')->insertGetId(array(
                'user_id' => $user_2,
                'price' => 5.50,
                'status' => 'PENDING',
                'description' => 'abjbiawj bbaiwr ubahwrhuh awrhhuawr haiwhr',
            ));
            $invoice_3 = DB::table('invoice')->insertGetId(array(
                'user_id' => $user_3,
                'price' => 10.50,
                'status' => 'PENDING',
                'description' => 'chicsohi hieiosh hioes',
            ));
            $invoice_4 = DB::table('invoice')->insertGetId(array(
                'user_id' => $user_4,
                'price' => 2.50,
                'status' => 'FAILURE',
                'description' => 'nnngsbjkb bskgbb bksgb',
            ));
            $invoice_5 = DB::table('invoice')->insertGetId(array(
                'user_id' => $user_5,
                'price' => 1.50,
                'status' => 'FAILURE',
                'description' => 'zdfojjfzd jofzdj ojzfd',
            ));

            DB::table('invoice_line')->insert(array(
                'invoice_id' => $invoice_1,
                'product_id' => $prod_id_1,
                'quantity' => 6
            ));
            DB::table('invoice_line')->insert(array(
                'invoice_id' => $invoice_2,
                'product_id' => $prod_id_2,
                'quantity' => 3
            ));
            DB::table('invoice_line')->insert(array(
                'invoice_id' => $invoice_3,
                'product_id' => $prod_id_3,
                'quantity' => 7
            ));
            DB::table('invoice_line')->insert(array(
                'invoice_id' => $invoice_4,
                'product_id' => $prod_id_4,
                'quantity' => 2
            ));
            DB::table('invoice_line')->insert(array(
                'invoice_id' => $invoice_5,
                'product_id' => $prod_id_5,
                'quantity' => 4
            ));

            DB::table('media')->insert(array(
                'name' => 'bjfsjdkb bsfsf'.$i,
                'alt' => 'jeajrbejak beajrbb jkaebjrjkb earb',
                'extension' => '.jpg',
                'type' => 'Image',
                'path' => 'sdfijifds/dsf/sdf'
            ));
            DB::table('media')->insert(array(
                'name' => 'oesoroeso'.$i,
                'alt' => 'oerosoeroserjoser',
                'extension' => '.jpeg',
                'type' => 'Image',
                'path' => 'sdfijawesdf'
            ));
            DB::table('media')->insert(array(
                'name' => 'cwewce bsfsf'.$i,
                'alt' => 'jeajrbejak beajrbb cccc earb',
                'extension' => '.png',
                'type' => 'Image',
                'path' => 'ad/dsf/sdf'
            ));
            DB::table('media')->insert(array(
                'name' => 'ppokwae bsfsf'.$i,
                'alt' => 'jeajrbejak beajrbb jkaebjrjkb earbaw ewae wae',
                'extension' => '.jpg',
                'type' => 'Image',
                'path' => 'oooofds/dsf/sdf'
            ));
            DB::table('media')->insert(array(
                'name' => 'zoweowoweo'.$i,
                'alt' => 'zdjfjozf jezrjoejzr',
                'extension' => '.jpeg',
                'type' => 'Image',
                'path' => 'abawe/awes'
            ));
        }
          
    
    }

    public function selectByID($loop_counter){
         DB::table('product')->where('id','114698')->get();
    }

    public function selectByName($loop_counter){
         DB::table('product')->where('name','appelsEnPeren')->get();
    }

    public function selectWherePrice($loop_counter){
         DB::table('product')->where('price','>','5.00')->get();
    }

    public function updateByID($loop_counter){
        DB::table('product')->where('id','114704')->update(['price' => '100.00']);
    }

    // public function updateByID($loop_counter){
    //     DB::table('product')->where('id','6840203')->update(['name' => 'appelsEnPeren']);
    // }
    
    public function updateByName($loop_counter){
        DB::table('product')->where('name','appelsEnPeren')->update(['price' => '100.00']);
    }


    public function joinTables($loop_counter){

      return   DB::table('user')
        ->join('naw', 'user.id', '=', 'naw.user_id')
        ->join('invoice', 'naw.user_id', '=', 'invoice.user_id')
        ->join('invoice_line', 'invoice_line.invoice_id', '=', 'invoice.id')
        ->join('product', 'product.id', '=', 'invoice_line.product_id')
        ->join('product_category', 'product.id', '=', 'product_category.product_id')
        ->join('category', 'category.id', '=', 'product_category.category_id')
        ->get();
    }

    public function simpleInsert($loop_counter){
           for($i = 0; $i < $loop_counter; $i++) {
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert One',
                'price' => 10.10,
                'btw' => 1
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert Two',
                'price' => 12.10,
                'btw' => 2
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert three',
                'price' => 15.10,
                'btw' => 3
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert four',
                'price' => 1.14,
                'btw' => 4
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert five',
                'price' => 1.30,
                'btw' => 5
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert six',
                'price' => 5.11,
                'btw' => 6
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert seven',
                'price' => 2.25,
                'btw' => 7
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert eight',
                'price' => 3.55,
                'btw' => 8
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert nine',
                'price' => 9.99,
                'btw' => 9
            ]);
            $insertOne = DB::table('product')->insert([
                'name' => 'simple insert ten',
                'price' => 6.66,
                'btw' => 10
            ]);
        }
    }

    public function dropAll(){
        DB::table('category')->delete();
        DB::table('product')->delete();
        DB::table('invoice')->delete();
        DB::table('user')->delete();
        DB::table('media')->delete();
    }

}
