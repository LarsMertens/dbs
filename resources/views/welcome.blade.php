<!DOCTYPE html>
<html ng-app="App">
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{url('bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{url('font-awesome.min.css')}}">

        <script src="{{ url('jquery.min.js') }}"></script>
        <script src="{{ url('angular/angular.min.js') }}"></script>

        <script src="{{ url('humanize-duration.js') }}"></script>
        <script src="{{ url('moment.js') }}"></script>
        <script src="{{ url('angular/angular-timer.min.js') }}"></script>

        <script src="{{ url('angular/app.js')}}"></script>
        <script src="{{ url('angular/controllers/SQLController.js') }}"></script>
        <script src="{{ url('angular/services/SQLService.js') }}"></script>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .button{
                background-color: #eb0060;
                color: #ffffff;
                padding: 10px 15px;
                margin-bottom: 5px;
                font-weight: bold;
            }
            .button:hover{
                cursor: pointer;
                background-color: #cf0055;
            }
            .results{
                margin-top: 100px;
            }
            .results .title{
                font-size: 6rem;
                text-align: center;
                font-weight: bold;
            }
            .results .results-wrapper{
                margin-top: 20px;
            }
            .results .result{
                font-size: 3rem;
                font-weight: bold;
            }
            .results .highlight{
               color: #eb0060;
            }
            input{
                width: 100%;
                padding: 10px 15px;
                margin-bottom: 10px;
                text-align: center;
                font-size: 1.5rem;
                font-weight: bold;
            }
            .database{
                font-size: 2rem;
                font-weight: bold;
                margin-bottom: 30px;
            }
            .load-icon{
                margin-top: 30px;
                font-size: 4rem;
            }
        </style>
    </head>
    <body ng-controller="SQLController">
        <div class="container">
            <div class="col-xs-3 col-xs-push-1">
                <div class="database">Database: MySQL</div>
                <input ng-model="loop_counter" ng-init="loop_counter=2000" placeholder="Enter number of loops..." type="text">
                <div ng-click="insert()" class="col-xs-12 no-padding button">INSERT</div>
                <div ng-click="selectByID()" class="col-xs-12 no-padding button">SELECT BY ID</div>
                <div ng-click="selectByName()" class="col-xs-12 no-padding button">SELECT BY NAME</div>
                <div ng-click="selectWherePrice()" class="col-xs-12 no-padding button">SELECT WHERE PRICE</div>
                <div ng-click="updateByID()" class="col-xs-12 no-padding button">UPDATE BY ID</div>
                <div ng-click="updateByName()" class="col-xs-12 no-padding button">UPDATE BY NAME</div>
                <div ng-click="joinTables()" class="col-xs-12 no-padding button">JOIN TABLES</div>
                <div ng-click="dropAll()" class="col-xs-12 no-padding button">DROP ALL</div>
                <div ng-click="simpleInsert()" class="col-xs-12 no-padding button">SimpleInsert</div>
            </div>
            <div class="results col-xs-7 col-xs-push-1">
                <div class="title col-xs-12">Results</div>
                <div class="results-wrapper col-xs-12">
                    <div class="result col-xs-12">
                        <span class="highlight">Execution Time:</span>
                        <timer autostart="false">
                            <br/>
                            <%hours%> hours
                            <%minutes%> minutes,
                            <%seconds%> seconds,
                            <%millis%> milliseconds.
                        </timer>
                    </div>
                </div>
                <p ng-if="loading">
                    <span class="load-icon fa fa-spinner fa-pulse fa-3x fa-fw"></span>
                </p>
            </div>
        </div>
    </body>
</html>
